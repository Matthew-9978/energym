import 'package:energym_test_project/src/presentation/constants/common/tags.dart';
import 'package:energym_test_project/src/presentation/pages/shop/widgets/products_widget.dart';
import 'package:energym_test_project/src/presentation/pages/splash/splash_page.dart';
import 'package:energym_test_project/src/presentation/ui/components/cart_icon.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:energym_test_project/main.dart' as app;

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  group('cart test:', () {
    testWidgets('add product', (tester) async {
      await app.main();

      await tester.pumpWidget(const SplashPage());

      await tester.pumpAndSettle();

      Finder productsFinder = find.byType(ProductWidget);

      expect(productsFinder, findsWidgets);

      /// Select a product that is not "out of stock" and add it to cart
      for (Element deviceElement in productsFinder.evaluate()) {
        final productWidget = (deviceElement.widget as ProductWidget);
        if (productWidget.product.inventory != 0) {
          Finder addButtonFinder =
              find.byKey(Key("add-button-${productWidget.product.id}"));
          expect(addButtonFinder, findsOneWidget);
          await tester.tap(addButtonFinder);
          break;
        }
      }

      await tester.pumpAndSettle();

      Finder cartProductsCountText = find.byKey(const Key(Constants.productsCountKey));

      expect(cartProductsCountText, findsOneWidget);

      Finder cartIcon = find.byType(CartIcon);

      expect(cartProductsCountText, findsOneWidget);

      expect((cartProductsCountText.evaluate().first.widget as Text).data, "1");

      await tester.tap(cartIcon);

      await tester.pumpAndSettle();

      Finder cartProductsFinder = find.byType(ProductWidget);

      expect(cartProductsFinder, findsWidgets);

      Finder saveOrderButton = find.byKey(const Key(Constants.saveOrderButton));

      expect(saveOrderButton, findsOneWidget);

      await tester.tap(saveOrderButton);

      await tester.pumpAndSettle();

      Finder dialogConfirmationButton = find.byKey(const Key(Constants.confirmDialogButton));

      expect(saveOrderButton, findsOneWidget);

      await tester.tap(dialogConfirmationButton);

      await tester.pumpAndSettle();

      cartProductsFinder = find.byType(ProductWidget);

      expect(cartProductsFinder, findsNothing);
    });
  });
}
