import 'package:energym_test_project/src/data/entity/api_response.dart';
import 'package:energym_test_project/src/data/entity/product/product_entity.dart';

abstract class ProductApi {
  Future<ApiResponse<List<ProductEntity>>> getProducts();
}