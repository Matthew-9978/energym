import 'package:energym_test_project/src/core/network/api_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:kiwi/kiwi.dart';

abstract class BaseApi {
  @protected
  final apiService = KiwiContainer().resolve<ApiService>();
}
