import 'package:energym_test_project/src/data/entity/cart/cart_entity.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:json_annotation/json_annotation.dart';

part 'order_entity.g.dart';

@HiveType(typeId: 3)
@JsonSerializable()
class OrderEntity {
  @HiveField(0)
  @JsonKey(name: "cart",toJson: cartToJson)
  CartEntity? cartEntity;

  @HiveField(1)
  @JsonKey(name: "created_at")
  DateTime? createdAt;

  OrderEntity({
    this.cartEntity,
    this.createdAt,
  });

  Map<String, dynamic> toJson() => _$OrderEntityToJson(this);

  factory OrderEntity.fromJson(Map<String, dynamic> json) =>
      _$OrderEntityFromJson(json);

  static cartToJson(CartEntity? cartEntity) {
    return cartEntity?.toJson();
  }
}
