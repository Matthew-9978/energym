import 'package:energym_test_project/src/data/entity/product/product_entity.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:json_annotation/json_annotation.dart';

part 'cart_entity.g.dart';

@HiveType(typeId: 1)
@JsonSerializable()
class CartEntity {
  @HiveField(0,defaultValue: [])
  @JsonKey(name: "products",toJson: productToJson)
  List<ProductEntity>? products;

  @HiveField(1,defaultValue: 0)
  @JsonKey(name: "total_price")
  int? totalPrice;

  CartEntity({
    this.products,
    this.totalPrice,
  });

  Map<String, dynamic> toJson() => _$CartEntityToJson(this);

  factory CartEntity.fromJson(Map<String, dynamic> json) =>
      _$CartEntityFromJson(json);


  static productToJson(List<ProductEntity>? product) {
    return product?.map((e) => e.toJson()).toList();
  }

}
