import 'package:hive_flutter/adapters.dart';
import 'package:json_annotation/json_annotation.dart';

part 'product_entity.g.dart';

@HiveType(typeId: 2)
@JsonSerializable()
class ProductEntity {
  @HiveField(0)
  @JsonKey(name: "id")
  int? id;

  @HiveField(1)
  @JsonKey(name: "title")
  String? title;

  @HiveField(2)
  @JsonKey(name: "price")
  int? price;

  @HiveField(3)
  @JsonKey(name: "inventory")
  int? inventory;


  @HiveField(4)
  @JsonKey(name: "image")
  String? image;

  @HiveField(5)
  @JsonKey(name: "description")
  String? description;

  @HiveField(6)
  @JsonKey(name: "count")
  int? count;

  ProductEntity({
    this.id,
    this.title,
    this.price,
    this.image,
    this.description,
    this.inventory,
    this.count,
  });

  Map<String, dynamic> toJson() => _$ProductEntityToJson(this);

  factory ProductEntity.fromJson(Map<String, dynamic> json) =>
      _$ProductEntityFromJson(json);
}
