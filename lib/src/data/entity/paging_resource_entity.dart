

class PaginationResourceEntity<T> {
  List<T> data = [];

  PaginationResourceEntity({
    this.data = const [],
  });

  PaginationResourceEntity.fromJson(
      dynamic json, T Function(dynamic) dataFromJson) {
    if (json['data'] != null) {
      json['data'].forEach((v) {
        data.add(dataFromJson(v));
      });
    }
  }


  Map<String, dynamic> toJson(
      Map<String, dynamic> Function(dynamic) dataToJson) {
    final map = <String, dynamic>{};
    map['data'] = data.map((v) => dataToJson(v)).toList();
    return map;
  }
}
