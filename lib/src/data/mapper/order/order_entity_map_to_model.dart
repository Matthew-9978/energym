import 'package:energym_test_project/src/data/entity/order/order_entity.dart';
import 'package:energym_test_project/src/data/mapper/cart/cart_entity_map_to_model.dart';
import 'package:energym_test_project/src/domain/model/order/order_model.dart';

extension OrderEntityMapper on OrderEntity {
  OrderModel mapToModel() {
    return OrderModel(
      createdAt: createdAt,
      cart: cartEntity?.mapToModel(),
    );
  }
}
