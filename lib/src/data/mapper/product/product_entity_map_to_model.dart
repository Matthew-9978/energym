import 'package:energym_test_project/src/data/entity/product/product_entity.dart';
import 'package:energym_test_project/src/domain/model/product/product_model.dart';

extension ProductEntityMapper on ProductEntity{
  ProductModel mapToModel(){
    return ProductModel(
      image: image,
      id: id,
      title: title,
      description: description,
      inventory: inventory,
      price: price,
      count: count ?? 0,
    );
  }
}