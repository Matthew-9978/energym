import 'package:energym_test_project/src/data/entity/cart/cart_entity.dart';
import 'package:energym_test_project/src/data/mapper/product/product_entity_map_to_model.dart';
import 'package:energym_test_project/src/domain/model/cart/cart_model.dart';

extension CartEntityMapper on CartEntity {
  CartModel mapToModel() {
    return CartModel(
      products: products
              ?.map((productEntity) => productEntity.mapToModel())
              .toList() ??
          [],
      totalPrice: totalPrice ?? 0,
    );
  }
}
