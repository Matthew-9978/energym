import 'package:dio/dio.dart';
import 'package:energym_test_project/src/core/network/api_provider.dart';
import 'package:energym_test_project/src/core/network/dio/dio_api_provider_impl.dart';
import 'package:energym_test_project/src/core/network/dio/dio_wrapper.dart';
import 'package:kiwi/kiwi.dart';

class NetworkModule {
  static Future<void> inject() async {
    KiwiContainer()
        .registerFactory<ApiService>((container) => ApiServiceImpl());

    KiwiContainer().registerSingleton<Dio>((container) => DioWrapper.provide());
  }
}
