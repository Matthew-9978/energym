import 'package:energym_test_project/src/core/storage/hive/hive_loal_Storage_impl.dart';
import 'package:energym_test_project/src/core/storage/local_storage.dart';
import 'package:energym_test_project/src/data/entity/cart/cart_entity.dart';
import 'package:energym_test_project/src/data/entity/order/order_entity.dart';
import 'package:energym_test_project/src/data/entity/product/product_entity.dart';
import 'package:energym_test_project/src/presentation/constants/common/tags.dart';
import 'package:flutter/foundation.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:kiwi/kiwi.dart';
import 'package:path_provider/path_provider.dart';

class StorageModule {
  static Future<void> inject() async {
    await _injectSingletons();
  }

  static Future<void> _injectSingletons() async {
    await _injectAndInitHive();
  }

  static Future<void> _openHiveBoxes() async {
    await Hive.openBox(Constants.cartBox);
    await Hive.openBox(Constants.ordersBox);
  }

  static Future<void> _injectAndInitHive() async {
    if (!kIsWeb) {
      var dir = await getApplicationDocumentsDirectory();
      await Hive.initFlutter(dir.path);
    } else {
      await Hive.initFlutter();
    }
    Hive
      ..registerAdapter(CartEntityAdapter())
      ..registerAdapter(ProductEntityAdapter())
      ..registerAdapter(OrderEntityAdapter());
    KiwiContainer().registerSingleton<MyHive>((container) => HiveImpl());
    await _openHiveBoxes();
  }
}
