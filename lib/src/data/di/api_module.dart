import 'package:energym_test_project/src/data/api/product_api/product_api.dart';
import 'package:energym_test_project/src/data/api/product_api/product_api_impl.dart';
import 'package:kiwi/kiwi.dart';

class ApiModule {
  static Future<void> inject() async {
    KiwiContainer().registerFactory<ProductApi>((container) => ProductApiImpl());
  }
}
