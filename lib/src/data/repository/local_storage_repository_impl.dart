import 'package:energym_test_project/src/core/storage/local_storage.dart';
import 'package:energym_test_project/src/data/entity/cart/cart_entity.dart';
import 'package:energym_test_project/src/data/mapper/cart/cart_entity_map_to_model.dart';
import 'package:energym_test_project/src/data/mapper/order/order_entity_map_to_model.dart';
import 'package:energym_test_project/src/domain/mapper/cart/cart_model_map_to_entity.dart';
import 'package:energym_test_project/src/domain/mapper/order/order_model_map_to_entity.dart';
import 'package:energym_test_project/src/domain/model/cart/cart_model.dart';
import 'package:energym_test_project/src/domain/model/order/order_model.dart';
import 'package:energym_test_project/src/domain/repository/local_repository/local_storage_repository.dart';


class LocalStorageRepositoryImpl extends LocalStorageRepository {
  final MyHive hive;

  LocalStorageRepositoryImpl({
    required this.hive,
  });

  @override
  Future<void> clearLocalStorage() async {
    await hive.clearAll();
  }

  @override
  Future<CartModel> getCart() async {
    final cartEntity = await hive.getCart();
    return cartEntity.mapToModel();
  }

  @override
  Future<void> updateCart(CartModel cartModel) async {
    CartModel oldCart = await getCart();
    Map<String, dynamic> oldObj = oldCart.mapToEntity().toJson();
    Map<String, dynamic> newObj = cartModel.mapToEntity().toJson();
    oldObj.forEach((key, value) {
      if (newObj[key] != null) {
        oldObj[key] = newObj[key];
      }
    });
    await hive.saveCart(CartEntity.fromJson(oldObj));
  }

  @override
  Future<List<OrderModel>> getOrders() async {
    final orders = await hive.getOrders();
    return orders.map((orderEntity) => orderEntity.mapToModel()).toList();
  }

  @override
  Future<void> saveOrder(OrderModel orderModel) async {
    final orders = await hive.getOrders();
    orders.add(orderModel.mapToEntity());
    await hive.saveOrder(orders);
  }
}
