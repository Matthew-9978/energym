import 'package:energym_test_project/src/data/api/product_api/product_api.dart';
import 'package:energym_test_project/src/data/mapper/product/product_entity_map_to_model.dart';
import 'package:energym_test_project/src/domain/model/product/product_model.dart';
import 'package:energym_test_project/src/domain/model/result_model.dart';
import 'package:energym_test_project/src/domain/repository/product_repositroy/product_repository.dart';

class ProductRepositoryImpl extends ProductRepository {
  final ProductApi productApi;

  ProductRepositoryImpl({
    required this.productApi,
  });

  @override
  Future<ResultModel<List<ProductModel>>> getProduct() async {
    final result = await productApi.getProducts();
    return ResultModel(
      data: result.data?.map((productEntity) => productEntity.mapToModel()).toList(),
      status: ResultStatus.success,
      statusCode: result.statusCode,
    );
  }
}
