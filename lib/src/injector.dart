
import 'package:energym_test_project/src/data/di/api_module.dart';
import 'package:energym_test_project/src/data/di/network_module.dart';
import 'package:energym_test_project/src/data/di/storage_module.dart';
import 'package:energym_test_project/src/domain/di/repository_module.dart';
import 'package:energym_test_project/src/presentation/di/presentation_injection.dart';

class Injector {
  static inject() async {
    await injectData();
    await _injectDomain();
    await _injectPresentation();
  }

  static Future<void> injectData() async {
    await ApiModule.inject();
    await NetworkModule.inject();
    await StorageModule.inject();
  }

  static _injectDomain() async {
    await RepositoryModule.inject();
  }

  static _injectPresentation() {
    PresentationInjection.inject();
  }
}