import 'package:dio/dio.dart';
import 'package:energym_test_project/src/core/network/api_provider.dart';
import 'package:kiwi/kiwi.dart';

class ApiServiceImpl extends ApiService {
  @override
  Future<Response> get(String path,
      {Map<String, dynamic>? queryParameter,
      Map<String, dynamic>? headerParameters}) async {
    final dio = KiwiContainer().resolve<Dio>();

    if (headerParameters?.isNotEmpty == true) {
      dio.options.headers.addAll(headerParameters!);
    }

    return await dio.get(
      path,
      queryParameters: queryParameter,
    );
  }

  @override
  Future<Response> download(
    String path,
    String savePath, {
    Map<String, dynamic>? queryParameter,
    Map<String, dynamic>? headerParameters,
    ProgressCallback? onReceiveProgress,
    CancelToken? cancelToken,
  }) async {
    final dio = KiwiContainer().resolve<Dio>();

    if (headerParameters?.isNotEmpty == true) {
      dio.options.headers.addAll(headerParameters!);
    }

    return await dio.download(
      path,
      savePath,
      queryParameters: queryParameter,
      onReceiveProgress: onReceiveProgress,
      cancelToken: cancelToken,
    );
  }

  @override
  Future<Response> patch(String path,
      {Map<String, dynamic>? queryParameter,
      Map<String, dynamic>? headerParameters,
      dynamic bodyParameters}) async {
    final dio = KiwiContainer().resolve<Dio>();

    if (headerParameters?.isNotEmpty == true) {
      dio.options.headers.addAll(headerParameters!);
    }
    return await dio.patch(
      path,
      queryParameters: queryParameter,
      data: bodyParameters,
    );
  }

  @override
  Future<Response> post(String path,
      {Map<String, dynamic>? queryParameter,
      Map<String, dynamic>? headerParameters,
      dynamic bodyParameters,
      ProgressCallback? onSendProgress,
      CancelToken? cancelToken}) async {
    final dio = KiwiContainer().resolve<Dio>();
    if (headerParameters?.isNotEmpty == true) {
      dio.options.headers.addAll(headerParameters!);
    }
    return await dio.post(path,
        queryParameters: queryParameter,
        data: bodyParameters,
        onSendProgress: onSendProgress,
        cancelToken: cancelToken);
  }

  @override
  Future<Response> put(String path,
      {Map<String, dynamic>? queryParameter,
      Map<String, dynamic>? headerParameters,
      dynamic bodyParameters}) async {
    final dio = KiwiContainer().resolve<Dio>();

    if (headerParameters?.isNotEmpty == true) {
      dio.options.headers.addAll(headerParameters!);
    }
    return await dio.put(
      path,
      queryParameters: queryParameter,
      data: bodyParameters,
    );
  }

  @override
  Future<Response> delete(String path,
      {Map<String, dynamic>? queryParameter,
      Map<String, dynamic>? headerParameters,
      dynamic bodyParameters}) async {
    final dio = KiwiContainer().resolve<Dio>();
    if (headerParameters?.isNotEmpty == true) {
      dio.options.headers.addAll(headerParameters!);
    }
    return await dio.delete(path,
        queryParameters: queryParameter, data: bodyParameters);
  }
}
