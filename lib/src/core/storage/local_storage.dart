import 'dart:async';

import 'package:energym_test_project/src/data/entity/cart/cart_entity.dart';
import 'package:energym_test_project/src/data/entity/order/order_entity.dart';
import 'package:mockito/annotations.dart';

@GenerateMocks([MyHive])
abstract class MyHive {
  Future<void> clearAll();

  Future<CartEntity> getCart();

  Future<void> saveCart(CartEntity cartEntity);

  Future<void> saveOrder(List<OrderEntity> orders);

  Future<List<OrderEntity>> getOrders();
}
