import 'package:energym_test_project/src/core/storage/local_storage.dart';
import 'package:energym_test_project/src/data/entity/cart/cart_entity.dart';
import 'package:energym_test_project/src/data/entity/order/order_entity.dart';
import 'package:energym_test_project/src/presentation/constants/common/tags.dart';
import 'package:hive_flutter/hive_flutter.dart';

class HiveImpl extends MyHive {
  @override
  Future<void> clearAll() async {
    await Hive.deleteFromDisk();
  }

  @override
  Future<CartEntity> getCart() async {
    final box = await Hive.openBox(Constants.cartBox);
    return await box.get(Constants.cartBox) ?? CartEntity(
      products: []
    );
  }

  @override
  Future<void> saveCart(CartEntity cartEntity) async {
    final box = await Hive.openBox(Constants.cartBox);
    await box.put(Constants.cartBox,cartEntity);
  }

  @override
  Future<List<OrderEntity>> getOrders() async {
    final box = await Hive.openBox(Constants.ordersBox);
    final list = await box.get(Constants.ordersBox,defaultValue: []) as List;
    List<OrderEntity> orders = [];
    for (var element in list) {
      if(element is OrderEntity){
        orders.add(element);
      }
    }
    return orders;
  }

  @override
  Future<void> saveOrder(List<OrderEntity> orders) async {
    final box = await Hive.openBox(Constants.ordersBox);
    await box.put(Constants.ordersBox,orders);
  }

}
