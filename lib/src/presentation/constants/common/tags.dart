class Constants{
  static const cartBox = "cart_box";
  static const ordersBox = "orders_box";
  static const productsCountKey = "products_count_key";
  static const saveOrderButton = "save_order_button";
  static const confirmDialogButton = "confirm_dialog_button";
}