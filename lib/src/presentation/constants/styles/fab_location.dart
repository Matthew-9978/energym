import 'dart:math' as math;

import 'package:flutter/material.dart';

class DockedRightFabLocation extends StandardFabLocation {
  final double? sizeFromBottom;

  DockedRightFabLocation({
    this.sizeFromBottom,
  });

  @override
  double getOffsetY(
      ScaffoldPrelayoutGeometry scaffoldGeometry, double adjustment) {
    final double contentBottom = scaffoldGeometry.contentBottom;
    final double bottomContentHeight =
        scaffoldGeometry.scaffoldSize.height - contentBottom;
    final double bottomSheetHeight = scaffoldGeometry.bottomSheetSize.height;
    final double fabHeight = scaffoldGeometry.floatingActionButtonSize.height;
    final double snackBarHeight = scaffoldGeometry.snackBarSize.height;
    final double safeMargin = math.max(
      kFloatingActionButtonMargin,
      scaffoldGeometry.minViewPadding.bottom -
          bottomContentHeight +
          kFloatingActionButtonMargin,
    );

    double fabY = contentBottom - fabHeight - safeMargin - (sizeFromBottom ?? 70);
    if (snackBarHeight > 0.0) {
      fabY = math.min(
          fabY,
          contentBottom -
              snackBarHeight -
              fabHeight -
              kFloatingActionButtonMargin);
    }
    if (bottomSheetHeight > 0.0) {
      fabY = math.min(contentBottom, 100 - bottomSheetHeight - fabHeight / 2.0);
    }

    return fabY + adjustment;
  }

  @override
  double getOffsetX(
      ScaffoldPrelayoutGeometry scaffoldGeometry, double adjustment) {
    return scaffoldGeometry.scaffoldSize.width -
        kFloatingActionButtonMargin -
        scaffoldGeometry.minInsets.right -
        scaffoldGeometry.floatingActionButtonSize.width +
        adjustment;
  }
}
