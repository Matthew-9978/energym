import 'package:energym_test_project/src/presentation/helpers/extensions/context_extensions.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/skeleton_widget.dart';
import 'package:flutter/material.dart';

class ShopSkeletonList extends StatelessWidget {
  const ShopSkeletonList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      itemCount: 10,
      separatorBuilder: (context, index) => const SizedBox(height: 8,),
      itemBuilder: (context, index) => SkeletonWidget.rectangular(
          width: context.screenWidth, height: context.screenHeight),
    );
  }
}
