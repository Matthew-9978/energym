import 'package:energym_test_project/src/domain/model/order/order_model.dart';
import 'package:energym_test_project/src/presentation/helpers/extensions/datetime_extension.dart';
import 'package:energym_test_project/src/presentation/helpers/extensions/int_extensions.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/background_widget.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/text_widget.dart';
import 'package:flutter/material.dart';

class OrdersItemWidget extends StatelessWidget {
  final OrderModel order;
  final int index;

  const OrdersItemWidget({
    Key? key,
    required this.order,
    required this.index,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BackgroundWidget(
      borderRadius: 16,
      child: ExpansionTile(
        childrenPadding:
            const EdgeInsets.symmetric(vertical: 16, horizontal: 16),
        title: Row(
          children: [
            Expanded(
              child: TextWidget.semiBold(
                "Order #$index",
                context: context,
                additionalStyle: TextStyle(
                  fontSize: 18,
                  color: Theme.of(context).colorScheme.onTertiary,
                ),
              ),
            ),
            TextWidget.semiBold(
              order.createdAt!.formattedDateWithTime,
              context: context,
              additionalStyle: TextStyle(
                fontSize: 16,
                color: Theme.of(context).colorScheme.onTertiary,
              ),
            ),
          ],
        ),
        children: [
          ...List.generate(
            order.cart?.products.length ?? 0,
            (index) => Padding(
              padding: const EdgeInsets.symmetric(vertical: 12.0),
              child: Row(
                children: [
                  Expanded(
                    child: TextWidget.medium(
                      order.cart?.products[index].title ?? "",
                      context: context,
                      additionalStyle: const TextStyle(
                        fontSize: 16,
                      ),
                    ),
                  ),
                  TextWidget.medium(
                    "${order.cart?.products[index].count} x ${order.cart?.products[index].price!.moneySeparation}",
                    context: context,
                    additionalStyle: const TextStyle(
                      fontSize: 16,
                    ),
                  )
                ],
              ),
            ),
          ),
          Divider(
            color: Theme.of(context).colorScheme.onSurfaceVariant,
          ),
          const SizedBox(height: 8,),
          Center(
            child: Column(
              children: [
                TextWidget.medium(
                  "total:",
                  context: context,
                  additionalStyle: const TextStyle(
                      fontSize: 18
                  ),
                ),
                const SizedBox(height: 4,),
                TextWidget.semiBold(
                  order.cart?.totalPrice.moneySeparation ?? "",
                  context: context,
                  additionalStyle: const TextStyle(
                    fontSize: 20
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
