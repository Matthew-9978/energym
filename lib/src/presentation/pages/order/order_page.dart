import 'package:energym_test_project/src/presentation/bloc/orders_cubit/orders_cubit.dart';
import 'package:energym_test_project/src/presentation/bloc/orders_cubit/orders_states.dart';
import 'package:energym_test_project/src/presentation/pages/order/widgets/order_widget.dart';
import 'package:energym_test_project/src/presentation/pages/shop/widgets/skeleton_loading.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/app_bar_widget.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/base_stateful_widget.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/text_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class OrdersPage extends BaseStatefulWidget<OrdersPage, OrdersCubit> {
  const OrdersPage({Key? key})
      : super(key: key, includeHorizontalPadding: true);

  @override
  State<OrdersPage> createState() => _OrdersPageState();
}

class _OrdersPageState
    extends BaseStatefulWidgetState<OrdersPage, OrdersCubit> {
  @override
  CustomAppbar? appBar(BuildContext context) {
    return CustomAppbar(
      title: "Orders",
      hasBackButton: true,
      onPressBack: () => Navigator.pop(context),
    );
  }

  @override
  void postFrameInitialization() {
    cubit.getOrders();
  }

  @override
  Widget body(BuildContext context) {
    return BlocBuilder<OrdersCubit, OrdersStates>(
      builder: (context, state) {
        if (state is GetOrdersState) {
          return state.when(
            loading: () => const ShopSkeletonList(),
            success: (orders) => Visibility(
              visible: orders.isNotEmpty,
              replacement: Center(
                child: TextWidget.semiBold(
                  "Cart is Empty",
                  context: context,
                  additionalStyle: const TextStyle(fontSize: 20),
                ),
              ),
              child: ListView.separated(
                itemCount: orders.length,
                separatorBuilder: (context, index) => const SizedBox(
                  height: 8,
                ),
                itemBuilder: (context, index) =>
                    OrdersItemWidget(order: orders[index], index: index + 1),
              ),
            ),
          );
        }
        return const SizedBox();
      },
    );
  }
}
