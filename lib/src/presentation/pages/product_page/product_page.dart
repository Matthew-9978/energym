import 'package:energym_test_project/src/presentation/bloc/product_cubit/product_cubit.dart';
import 'package:energym_test_project/src/presentation/constants/common/assets.dart';
import 'package:energym_test_project/src/presentation/helpers/extensions/context_extensions.dart';
import 'package:energym_test_project/src/presentation/pages/product_page/param/product_pag_params.dart';
import 'package:energym_test_project/src/presentation/pages/product_page/widgets/product_bottom_widget.dart';
import 'package:energym_test_project/src/presentation/ui/components/cart_icon.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/app_bar_widget.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/base_stateful_widget.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/image_widget.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/text_widget.dart';
import 'package:flutter/material.dart';

class ProductPage extends BaseStatefulWidget<ProductPage, ProductCubit> {
  final ProductPageParams? params;

  const ProductPage({
    Key? key,
    this.params,
  }) : super(
          key: key,
          includeHorizontalPadding: true,
          includeVerticalPadding: true,
        );

  @override
  State<ProductPage> createState() => _ProductPageState();
}

class _ProductPageState
    extends BaseStatefulWidgetState<ProductPage, ProductCubit> {
  @override
  void initState() {
    super.initState();
    cubit.initProductModel(widget.params?.productModel);
  }

  @override
  CustomAppbar? appBar(BuildContext context) {
    return CustomAppbar(
      title: cubit.product.title,
      hasBackButton: true,
      onPressBack: () => Navigator.pop(context),
      actions: const [
        CartIcon(),
      ],
    );
  }

  @override
  Widget? bottomWidget() {
    return ProductPageBottomWidget(product: cubit.product);
  }

  @override
  Widget body(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ImageLoaderWidget(
            imageUrl: Assets.appLogo,
            height: 300,
            width: context.screenWidth,
          ),
          const SizedBox(
            height: 16,
          ),
          TextWidget.bold(
            cubit.product.title ?? "",
            context: context,
            additionalStyle: const TextStyle(fontSize: 20),
          ),
          const SizedBox(
            height: 12,
          ),
          TextWidget.medium(
            cubit.product.description ?? "",
            context: context,
            textAlign: TextAlign.justify,
            additionalStyle: TextStyle(
                fontSize: 16, color: Theme.of(context).colorScheme.onTertiary),
          )
        ],
      ),
    );
  }
}
