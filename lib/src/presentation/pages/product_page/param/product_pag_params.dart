import 'package:energym_test_project/src/domain/model/product/product_model.dart';

class ProductPageParams {
  final ProductModel productModel;

  ProductPageParams({
    required this.productModel,
  });
}