import 'package:energym_test_project/src/domain/model/product/product_model.dart';
import 'package:energym_test_project/src/presentation/helpers/extensions/int_extensions.dart';
import 'package:energym_test_project/src/presentation/ui/components/product_purchase_button.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/text_widget.dart';
import 'package:flutter/material.dart';

class ProductPageBottomWidget extends StatelessWidget {
  final ProductModel product;

  const ProductPageBottomWidget({
    Key? key,
    required this.product,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16,vertical: 16),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.1),
            blurRadius: 10,
          )
        ],
      ),
      child: SafeArea(
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  TextWidget.medium(
                    "price:",
                    context: context,
                    additionalStyle: const TextStyle(fontSize: 16),
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  TextWidget.bold(
                    product.price?.moneySeparation ?? "",
                    context: context,
                    additionalStyle: const TextStyle(fontSize: 21),
                  ),
                ],
              ),
            ),
            SizedBox(
              width: 200,
              height: 40,
              child: ProductPurchaseWidget(product: product),
            )
          ],
        ),
      ),
    );
  }
}
