import 'package:energym_test_project/src/presentation/bloc/cart_cubit/cart_cubit.dart';
import 'package:energym_test_project/src/presentation/bloc/cart_cubit/cart_states.dart';
import 'package:energym_test_project/src/presentation/helpers/extensions/context_extensions.dart';
import 'package:energym_test_project/src/presentation/helpers/extensions/dart_extension.dart';
import 'package:energym_test_project/src/presentation/pages/cart/widgets/cart_bottom_widget.dart';
import 'package:energym_test_project/src/presentation/pages/shop/widgets/products_widget.dart';
import 'package:energym_test_project/src/presentation/ui/dialogs/default_dialog_style.dart';
import 'package:energym_test_project/src/presentation/ui/dialogs/dialog_function.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/app_bar_widget.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/base_stateful_widget.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/icon_widget.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/text_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CartPage extends BaseStatefulWidget<CartPage, CartCubit> {
  const CartPage({Key? key}) : super(key: key, includeHorizontalPadding: true);

  @override
  State<CartPage> createState() => _ShopPageState();
}

class _ShopPageState extends BaseStatefulWidgetState<CartPage, CartCubit> {
  @override
  CustomAppbar? appBar(BuildContext context) {
    return CustomAppbar(
      title: "Cart",
      hasBackButton: true,
      onPressBack: () => Navigator.pop(context),
    );
  }

  @override
  Widget? bottomWidget() {
    return BlocBuilder<CartCubit, CartStates>(
      builder: (context, state) => Visibility(
        visible: cubit.cart.products.isNotEmpty,
        child: CartBottomWidget(
          totalPrice: cubit.cart.totalPrice,
          onConfirmOrder: () {
            showOrderConfirmationDialog();
          },
        ),
      ),
    );
  }

  @override
  Widget body(BuildContext context) {
    return BlocConsumer<CartCubit, CartStates>(
      listener: _listenToConfirmOrderStates,
      listenWhen: _listenWhenToConfirmOrder,
      builder: (context, state) => Visibility(
        visible: cubit.cart.products.isNotEmpty,
        replacement: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              IconWidget(
                icon: Icons.remove_shopping_cart_outlined,
                size: 80,
                iconColor: Theme.of(context).colorScheme.onTertiary,
              ),
              const SizedBox(
                height: 16,
              ),
              TextWidget.semiBold(
                "Cart is Empty",
                context: context,
                additionalStyle: const TextStyle(fontSize: 20),
              )
            ],
          ),
        ),
        child: GridView.builder(
          gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
            maxCrossAxisExtent: 250,
            mainAxisExtent: 300,
            crossAxisSpacing: 8,
            mainAxisSpacing: 8,
          ),
          itemCount: cubit.cart.products.length,
          itemBuilder: (context, index) => ProductWidget(
            product: cubit.cart.products[index],
          ),
        ),
      ),
    );
  }

  bool _listenWhenToConfirmOrder(CartStates previous, CartStates current) {
    return current is ConfirmOrderStates;
  }

  void _listenToConfirmOrderStates(BuildContext context, CartStates state) {
    state.isA<ConfirmOrderStates>()?.whenOrNull(
        success: (){
          cubit.clearCart();
          Navigator.pop(context);
          context.showSuccessSnackBar("Your order saved successfully.");
        }
    );
  }

  void showOrderConfirmationDialog() {
    showCustomDialog(
      context,
      (context) => DialogDefaultStyle(
        title: "Order Confirmation",
        question: "Are you sure you want to save your order?",
        acceptButtonText: "confirm",
        onAccept: () async {
          await cubit.confirmOrder();
        },
        acceptButtonColor: Theme.of(context).colorScheme.surfaceVariant,
        onReject: () => Navigator.pop(context),
      ),
    );
  }


}
