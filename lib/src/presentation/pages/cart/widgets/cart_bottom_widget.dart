import 'package:energym_test_project/src/presentation/constants/common/tags.dart';
import 'package:energym_test_project/src/presentation/helpers/extensions/int_extensions.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/button_widget.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/text_widget.dart';
import 'package:flutter/material.dart';

class CartBottomWidget extends StatelessWidget {
  final int totalPrice;
  final VoidCallback onConfirmOrder;

  const CartBottomWidget({
    Key? key,
    required this.totalPrice,
    required this.onConfirmOrder,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
      decoration: BoxDecoration(color: Colors.white, boxShadow: [
        BoxShadow(
            blurRadius: 10,
            spreadRadius: 0,
            color: Colors.black.withOpacity(0.1))
      ]),
      child: SafeArea(
        child: Row(
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  TextWidget.medium(
                    "total:",
                    context: context,
                    additionalStyle: const TextStyle(fontSize: 16),
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  TextWidget.bold(
                    totalPrice.moneySeparation,
                    context: context,
                    additionalStyle: const TextStyle(fontSize: 21),
                  ),
                ],
              ),
            ),
            CustomButton.fill(
              context: context,
              width: 200,
              borderRadius: 8,
              backgroundColor: Theme.of(context).colorScheme.surfaceVariant,
              text: "Create order",
              key: const Key(Constants.saveOrderButton),
              onPressed: onConfirmOrder,
            )
          ],
        ),
      ),
    );
  }
}
