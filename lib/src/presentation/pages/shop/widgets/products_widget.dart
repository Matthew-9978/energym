import 'package:energym_test_project/src/domain/model/product/product_model.dart';
import 'package:energym_test_project/src/presentation/config/routes.dart';
import 'package:energym_test_project/src/presentation/constants/common/assets.dart';
import 'package:energym_test_project/src/presentation/helpers/extensions/context_extensions.dart';
import 'package:energym_test_project/src/presentation/helpers/extensions/int_extensions.dart';
import 'package:energym_test_project/src/presentation/pages/product_page/param/product_pag_params.dart';
import 'package:energym_test_project/src/presentation/ui/components/product_purchase_button.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/image_widget.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/text_widget.dart';
import 'package:flutter/material.dart';

class ProductWidget extends StatelessWidget {
  final ProductModel product;

  const ProductWidget({
    Key? key,
    required this.product,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    context.cartCubit.syncProductCount(product);
    return InkWell(
      onTap: () {
        Navigator.pushNamed(
          context,
          Routes.productPage,
          arguments: ProductPageParams(productModel: product),
        );
      },
      child: Container(
        padding: const EdgeInsets.all(4),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.1),
              blurRadius: 10,
              spreadRadius: 0,
              offset: const Offset(0, 0),
            )
          ],
        ),
        child: Column(
          children: [
            const Expanded(
              flex: 3,
              child: ImageLoaderWidget(
                imageUrl: Assets.appLogo,
              ),
            ),
            Expanded(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Expanded(
                          child: TextWidget.semiBold(
                            product.title ?? "",
                            context: context,
                            additionalStyle: const TextStyle(
                              fontSize: 16,
                            ),
                          ),
                        ),
                        TextWidget.semiBold(
                          product.price!.moneySeparation,
                          context: context,
                          additionalStyle: const TextStyle(
                            fontSize: 14,
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 4,
                    ),
                    Expanded(
                      child: TextWidget.regular(
                        product.description ?? "",
                        context: context,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        additionalStyle: const TextStyle(
                          fontSize: 12,
                        ),
                      ),
                    ),
                    ProductPurchaseWidget(product: product),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
