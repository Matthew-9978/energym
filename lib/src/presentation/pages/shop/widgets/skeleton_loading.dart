import 'package:energym_test_project/src/presentation/helpers/extensions/context_extensions.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/skeleton_widget.dart';
import 'package:flutter/material.dart';

class ShopSkeletonList extends StatelessWidget {
  const ShopSkeletonList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 250,
        mainAxisExtent: 300,
        crossAxisSpacing: 8,
        mainAxisSpacing: 8,
      ),
      itemCount: 10,
      itemBuilder: (context, index) => SkeletonWidget.rectangular(
          width: context.screenWidth, height: context.screenHeight),
    );
  }
}
