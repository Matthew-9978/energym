import 'package:energym_test_project/src/presentation/bloc/shop_cubit/shop_cubit.dart';
import 'package:energym_test_project/src/presentation/bloc/shop_cubit/shop_states.dart';
import 'package:energym_test_project/src/presentation/config/routes.dart';
import 'package:energym_test_project/src/presentation/helpers/extensions/context_extensions.dart';
import 'package:energym_test_project/src/presentation/pages/shop/widgets/products_widget.dart';
import 'package:energym_test_project/src/presentation/pages/shop/widgets/skeleton_loading.dart';
import 'package:energym_test_project/src/presentation/ui/components/cart_icon.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/app_bar_widget.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/background_widget.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/base_stateful_widget.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/error_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ShopPage extends BaseStatefulWidget<ShopPage, ShopCubit> {
  const ShopPage({Key? key}) : super(key: key, includeHorizontalPadding: true);

  @override
  State<ShopPage> createState() => _ShopPageState();
}

class _ShopPageState extends BaseStatefulWidgetState<ShopPage, ShopCubit> {
  @override
  CustomAppbar? appBar(BuildContext context) {
    return CustomAppbar(
      title: "Energym Shop",
      leading: Padding(
        padding: const EdgeInsets.only(left: 8.0),
        child: BackgroundWidget(
          isBordered: true,
          borderColor: Theme.of(context).colorScheme.outline,
          borderRadius: 8,
          verticalPadding: 8,
          horizontalPadding: 8,
          onTap: () => Navigator.pushNamed(context, Routes.ordersPage),
          child: Icon(
            Icons.newspaper_outlined,
            color: Theme.of(context).colorScheme.primary,
            size: 32,
          ),
        ),
      ),
      actions: const [
        CartIcon(),
      ],
    );
  }

  @override
  void postFrameInitialization() {
    cubit.getProducts();
  }

  @override
  Widget body(BuildContext context) {
    return BlocBuilder<ShopCubit, ShopStates>(
      builder: (context, state) {
        if (state is GetProductsState) {
          return state.when(
            loading: () => const ShopSkeletonList(),
            success: (products) => GridView.builder(
              gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                maxCrossAxisExtent: 250,
                mainAxisExtent: 300,
                crossAxisSpacing: 8,
                mainAxisSpacing: 8,
              ),
              itemCount: products.length,
              itemBuilder: (context, index) => ProductWidget(
                product: products[index],
                key: Key("${products[index].id}"),
              ),
            ),
            failed: (error) => CustomErrorWidget(
              error: error,
            ),
          );
        }
        return const SizedBox();
      },
    );
  }
}
