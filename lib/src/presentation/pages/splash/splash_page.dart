import 'package:energym_test_project/src/presentation/bloc/cart_cubit/cart_cubit.dart';
import 'package:energym_test_project/src/presentation/bloc/splash_cubit/splash_cubit.dart';
import 'package:energym_test_project/src/presentation/bloc/splash_cubit/splash_state.dart';
import 'package:energym_test_project/src/presentation/config/routes.dart';
import 'package:energym_test_project/src/presentation/constants/common/assets.dart';
import 'package:energym_test_project/src/presentation/helpers/extensions/context_extensions.dart';
import 'package:energym_test_project/src/presentation/helpers/extensions/dart_extension.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/app_bar_widget.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/base_stateful_widget.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/icon_widget.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/image_widget.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/loading_widget.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/text_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kiwi/kiwi.dart';

class SplashPage extends BaseStatefulWidget<SplashPage, SplashCubit> {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState
    extends BaseStatefulWidgetState<SplashPage, SplashCubit> {
  @override
  CustomAppbar? appBar(BuildContext context) {
    return CustomAppbar(
      statusBarColor: Theme.of(context).primaryColor,
      backgroundColor: Theme.of(context).primaryColor,
    );
  }

  @override
  void postFrameInitialization() {
    cubit.checkUserToken();
  }

  @override
  Color? backgroundColor(context) => Theme.of(context).primaryColor;

  @override
  Widget body(BuildContext context) {
    return BlocListener<SplashCubit, SplashState>(
      listener: _listenToUserAuthentication,
      listenWhen: _listenWhenToSplashState,
      child: Center(
        child: Column(
          children: [
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const ImageLoaderWidget.fromAsset(
                    imageUrl: Assets.appLogo,
                    width: 200,
                    height: 200,
                  ),
                  const SizedBox(
                    height: 24,
                  ),
                  TextWidget.bold(
                    context.getStrings.appName,
                    context: context,
                    additionalStyle: TextStyle(
                      color: Theme.of(context).colorScheme.background,
                    ),
                  ),
                  const SizedBox(
                    height: 32,
                  ),
                  const Loading(
                    primaryLoading: false,
                  ),
                ],
              ),
            ),
            FutureBuilder<String>(
              future: context.read<SplashCubit>().getAppVersion(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return TextWidget.bold(
                    "${context.getStrings.version} ${snapshot.data}",
                    context: context,
                    additionalStyle: TextStyle(
                      fontSize: 18,
                      color: Theme.of(context).colorScheme.background,
                    ),
                  );
                }
                return const SizedBox();
              },
            )
          ],
        ),
      ),
    );
  }

  bool _listenWhenToSplashState(SplashState previous, SplashState current) {
    return current is CheckUserSplashState;
  }

  void _listenToUserAuthentication(BuildContext context, SplashState state) {
    state.isA<CheckUserSplashState>()?.when(
          unAuthenticated: () {},
          authenticated: () {
            KiwiContainer().resolve<CartCubit>().getCartFromStorage();
            Navigator.pushNamedAndRemoveUntil(
                context, Routes.shopPage, (route) => false);
          },
        );
  }
}
