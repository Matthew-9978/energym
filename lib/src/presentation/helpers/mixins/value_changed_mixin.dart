mixin ValueChangeChecker<T> {
  T? initialValue;
  bool? hasChanged;

  void setInitValue(T? value){
    initialValue = value;
  }

  void setForceChanging(bool hasChanged){
    this.hasChanged = hasChanged;
  }

  bool isValueChanged(T? newValue){
    return hasChanged ?? initialValue.toString() != newValue.toString();
  }
}