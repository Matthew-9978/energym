extension StringExtension on String {
  String? get appendZeroPrefix {
    return length <= 1 ? "0$this" : this;
  }

  bool get isUrlFromNetwork{
    return startsWith("http");
  }

  String get getFileNameByPath{
    return split('/').last;
  }
}
