import 'package:flutter/material.dart';

extension AlignmentExtension on AlignmentGeometry {
  bool get isLeft {
    return this == Alignment.centerLeft ||
        this == Alignment.bottomLeft ||
        this == Alignment.topLeft;
  }

  bool get isRight {
    return this == Alignment.topRight ||
        this == Alignment.centerRight ||
        this == Alignment.bottomRight;
  }

  bool get isTop {
    return this == Alignment.topRight ||
        this == Alignment.topLeft ||
        this == Alignment.topCenter;
  }

  bool get isBottom {
    return this == Alignment.bottomRight ||
        this == Alignment.bottomLeft ||
        this == Alignment.bottomCenter;
  }
}
