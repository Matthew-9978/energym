import 'package:intl/intl.dart';

extension DateTimeExtension on DateTime {
  String get formattedDateWithTime {
    DateFormat dateFormat = DateFormat("yyyy-MM-dd hh:mm");
    return dateFormat.format(this);
  }
}
