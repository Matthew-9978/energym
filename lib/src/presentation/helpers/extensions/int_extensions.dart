import 'package:intl/intl.dart';

extension IntExtensions on int{
  String get moneySeparation{
    final formatter = NumberFormat('#,##0');
    return "${formatter.format(this)}\$";
  }
}