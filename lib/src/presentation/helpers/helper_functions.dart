import 'package:energym_test_project/src/presentation/constants/styles/text_styles.dart';
import 'package:flutter/material.dart';

String getFontFamily(BuildContext context) {
  if (Localizations.localeOf(context).languageCode == 'fa') {
    return Styles.persianFont;
  }
  return Styles.persianFont;
}

void postFrameCallback(VoidCallback callback) {
  WidgetsBinding.instance.addPostFrameCallback((timeStamp) => callback.call());
}
