import 'package:energym_test_project/src/presentation/bloc/cart_cubit/cart_cubit.dart';
import 'package:energym_test_project/src/presentation/config/navigation_observer.dart';
import 'package:kiwi/kiwi.dart';

class PresentationInjection {

  static void inject() {
    _injectSingletons();
  }

  static void _injectSingletons() {
    KiwiContainer().registerSingleton<NavigationObserver>((container) =>
        NavigationObserver());
    KiwiContainer().registerSingleton<CartCubit>((container) =>
        CartCubit(
            localStorageRepository: container.resolve()
        ));
  }
}