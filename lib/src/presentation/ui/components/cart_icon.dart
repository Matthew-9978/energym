import 'package:energym_test_project/src/presentation/bloc/cart_cubit/cart_cubit.dart';
import 'package:energym_test_project/src/presentation/bloc/cart_cubit/cart_states.dart';
import 'package:energym_test_project/src/presentation/config/routes.dart';
import 'package:energym_test_project/src/presentation/constants/common/tags.dart';
import 'package:energym_test_project/src/presentation/helpers/extensions/context_extensions.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/background_widget.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/tag_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CartIcon extends StatelessWidget {
  const CartIcon({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CartCubit, CartStates>(
      buildWhen: _buildWhenCartIcon,
      builder: (context, state) => Padding(
        padding: const EdgeInsets.only(left: 8),
        child: Stack(
          children: [
            BackgroundWidget(
              isBordered: true,
              borderColor: Theme.of(context).colorScheme.outline,
              borderRadius: 8,
              verticalPadding: 8,
              horizontalPadding: 8,
              onTap: () => Navigator.pushNamed(context, Routes.cartPage),
              child: Icon(
                Icons.shopping_cart_outlined,
                color: Theme.of(context).colorScheme.primary,
                size: 32,
              ),
            ),
            Align(
              alignment: Alignment.bottomRight,
              child: IgnorePointer(
                child: Visibility(
                  visible: context.cartCubit.productsCount != 0,
                  child: TagWidget.rectangle(
                    borderRadius: 2,
                    backgroundColor:
                        Theme.of(context).colorScheme.onSurfaceVariant,
                    child: Text(
                      context.cartCubit.productsCount.toString(),
                      key: const Key(Constants.productsCountKey),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  bool _buildWhenCartIcon(CartStates previous, CartStates current) {
    return current is UpdateCartState ||
        current is GetCartFromLocal ||
        current is ClearCartState;
  }
}
