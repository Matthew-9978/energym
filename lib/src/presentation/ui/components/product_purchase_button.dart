import 'package:energym_test_project/src/presentation/bloc/cart_cubit/cart_cubit.dart';
import 'package:energym_test_project/src/presentation/bloc/cart_cubit/cart_states.dart';
import 'package:energym_test_project/src/domain/model/product/product_model.dart';
import 'package:energym_test_project/src/presentation/helpers/extensions/context_extensions.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/button_widget.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/icon_widget.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/text_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ProductPurchaseWidget extends StatelessWidget {
  final ProductModel product;

  const ProductPurchaseWidget({
    Key? key,
    required this.product,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: product.inventory != 0,
      replacement: Center(
        child: TextWidget.medium("out of stock", context: context),
      ),
      child: BlocConsumer<CartCubit, CartStates>(
        listener: _listenToCartStates,
        listenWhen: _buildWhenProductButton,
        buildWhen: _buildWhenProductButton,
        builder: (context, state) {
          if (context.read<CartCubit>().isProductInCart(product)) {
            return Row(
              children: [
                IconWidget(
                  borderColor: Theme.of(context).colorScheme.outline,
                  icon: Icons.minimize,
                  iconColor: Theme.of(context).primaryColor,
                  hasBorder: true,
                  padding: 8,
                  borderRadius: 8,
                  onPressed: () {
                    context.cartCubit.removeProduct(product);
                  },
                ),
                Expanded(
                  child: Center(
                    child: TextWidget.bold(
                      product.count.toString(),
                      context: context,
                      additionalStyle: const TextStyle(fontSize: 16),
                    ),
                  ),
                ),
                IconWidget(
                  borderColor: Theme.of(context).colorScheme.outline,
                  icon: Icons.add,
                  iconColor: Theme.of(context).primaryColor,
                  hasBorder: true,
                  padding: 8,
                  borderRadius: 8,
                  onPressed: () {
                    if (product.inventory! <= product.count) {
                      context.showFailedSnackBar("The product is out of stock");
                    } else {
                      context.cartCubit.addProduct(product);
                    }
                  },
                )
              ],
            );
          }
          return CustomButton.outline(
            context: context,
            borderRadius: 8,
            borderColor: Theme.of(context).primaryColor,
            height: 40,
            key: Key("add-button-${product.id}"),
            additionalTextStyle: const TextStyle(fontSize: 14),
            text: "Add to Cart",
            onPressed: () {
              context.cartCubit.addProduct(product);
            },
          );
        },
      ),
    );
  }

  bool _buildWhenProductButton(CartStates previous, CartStates current) {
    if (current is UpdateCartState && current.productId == product.id) {
      return true;
    }
    return current is GetCartFromLocal || current is ClearCartState;
  }

  void _listenToCartStates(BuildContext context, CartStates state) {
    if (state is ClearCartState) {
      product.count = 0;
    }
    if (state is UpdateCartState && state.productId == product.id) {
      product.count = state.count;
    }
  }
}
