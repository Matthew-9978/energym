import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CupertinoSwitchWidget extends StatefulWidget {
  final bool isSelected;
  final Function(bool value) onChanged;

  const CupertinoSwitchWidget({
    Key? key,
    required this.onChanged,
    required this.isSelected,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _CupertinoSwitchWidgetState();
}

class _CupertinoSwitchWidgetState extends State<CupertinoSwitchWidget> {
  late bool isSelected;

  @override
  void initState() {
    isSelected = widget.isSelected;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoSwitch(
      key: widget.key,
      activeColor: Theme.of(context).colorScheme.primary,
      value: isSelected,
      onChanged: (value) {
        setState(() {
          isSelected = !isSelected;
        });
        widget.onChanged.call(isSelected);
      },
    );
  }
}
