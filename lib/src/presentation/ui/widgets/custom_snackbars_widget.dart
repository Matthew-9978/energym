import 'package:bot_toast/bot_toast.dart';
import 'package:energym_test_project/src/presentation/helpers/extensions/context_extensions.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/text_widget.dart';
import 'package:flutter/material.dart';

enum SnackType { failure, success, normal }

class CustomSnackBars {
  static showSnackBar({
    required SnackType snackType,
    required String title,
    required BuildContext context,
  }) {
    BotToast.showCustomNotification(
      toastBuilder: (cancelFunc) => _snackBarWidget(context, snackType, title),
      duration: const Duration(milliseconds: 2500),
      animationDuration: const Duration(milliseconds: 600),
      useSafeArea: true,
    );
  }

  static Widget _snackBarWidget(
      BuildContext context, SnackType snackType, String title) {
    return Card(
      color: Colors.white,
      margin: const EdgeInsets.symmetric(horizontal: 16),
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16),
        side: BorderSide(
          color: _snackTypeColor(context, snackType),
        ),
      ),
      child: Container(
        color: _snackTypeColor(context, snackType).withOpacity(0.1),
        padding: const EdgeInsets.symmetric(
          horizontal: 16,
          vertical: 16,
        ),
        width: context.screenWidth,
        child: TextWidget.medium(
          title,
          context: context,
          additionalStyle: TextStyle(
            fontSize: 14,
            color: _snackTypeColor(context, snackType),
          ),
        ),
      ),
    );
  }

  static Color _snackTypeColor(BuildContext context, SnackType snackType) {
    switch (snackType) {
      case SnackType.success:
        return Theme.of(context).colorScheme.surfaceVariant;
      default:
        return Theme.of(context).colorScheme.primary;
    }
  }

  static showSuccessSnackBar(BuildContext context, String title) {
    showSnackBar(snackType: SnackType.success, title: title, context: context);
  }

  static showFailedSnackBar(BuildContext context, String? title) {
    showSnackBar(
        snackType: SnackType.failure,
        title: title ?? context.getStrings.errorOccurred,
        context: context);
  }
}
