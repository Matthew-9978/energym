import 'package:flutter/material.dart';

class Loading extends StatelessWidget {
  final bool primaryLoading;
  final double? width;
  final double? height;

  const Loading({
    Key? key,
    this.primaryLoading = false,
    this.width = 28,
    this.height = 28,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
      width: width,
      child: CircularProgressIndicator(
        strokeWidth: 3,
        color:
            primaryLoading ? Theme.of(context).colorScheme.primary : Colors.white,

      ),
    );
  }
}
