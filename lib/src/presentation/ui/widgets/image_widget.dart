import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/skeleton_widget.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

enum ImageType { asset, file, network }

class ImageLoaderWidget extends StatelessWidget {
  final String imageUrl;
  final ImageType? imageType;
  final BoxShape boxShape;
  final BoxFit? boxFit;
  final double? height;
  final double? width;
  final double? borderRadius;
  final VoidCallback? onTap;
  final bool hasTag;
  final Widget? tagWidget;
  final Alignment? tagAlignment;
  final bool hasGradient;

  const ImageLoaderWidget({
    Key? key,
    required this.imageUrl,
    this.imageType,
    this.boxShape = BoxShape.rectangle,
    this.height,
    this.width,
    this.boxFit = BoxFit.cover,
    this.borderRadius,
    this.onTap,
    this.hasTag = false,
    this.tagWidget,
    this.tagAlignment,
    this.hasGradient = false,
  }) : super(key: key);

  const ImageLoaderWidget.fromAsset({
    Key? key,
    required this.imageUrl,
    this.boxShape = BoxShape.rectangle,
    this.height,
    this.width,
    this.boxFit = BoxFit.cover,
    this.borderRadius,
    this.onTap,
    this.hasTag = false,
    this.tagWidget,
    this.tagAlignment,
    this.hasGradient = false,
  })  : imageType = ImageType.asset,
        super(key: key);

  const ImageLoaderWidget.fromNetwork({
    Key? key,
    required this.imageUrl,
    this.boxShape = BoxShape.rectangle,
    this.height,
    this.width,
    this.boxFit = BoxFit.cover,
    this.borderRadius,
    this.onTap,
    this.hasTag = false,
    this.tagWidget,
    this.tagAlignment,
    this.hasGradient = false,
  })  : imageType = ImageType.network,
        super(key: key);

  const ImageLoaderWidget.fromFile({
    Key? key,
    required this.imageUrl,
    this.boxShape = BoxShape.rectangle,
    this.height,
    this.width,
    this.boxFit = BoxFit.cover,
    this.borderRadius,
    this.onTap,
    this.hasTag = false,
    this.tagWidget,
    this.tagAlignment,
    this.hasGradient = false,
  })  : imageType = ImageType.file,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height,
      width: width,
      child: GestureDetector(
        onTap: onTap,
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: [
            Positioned.fill(
              child: Container(
                clipBehavior: Clip.antiAlias,
                decoration: BoxDecoration(
                  borderRadius: boxShape == BoxShape.circle
                      ? null
                      : BorderRadius.circular(borderRadius ?? 0),
                  shape: boxShape,
                ),
                child: Builder(
                  builder: (context) {
                    switch (type) {
                      case ImageType.asset:
                        return Image.asset(imageUrl,
                            fit: boxFit,
                            width: width,
                            height: height,
                            errorBuilder: (context, error, stackTrace) =>
                                _errorWidget(context));
                      case ImageType.file:
                        return Image.file(File(imageUrl),
                            fit: boxFit,
                            width: width,
                            height: height,
                            errorBuilder: (context, error, stackTrace) =>
                                _errorWidget(context));
                      case ImageType.network:
                        return CachedNetworkImage(
                          imageUrl: imageUrl,
                          fit: boxFit,
                          errorWidget: (context, url, error) =>
                              _errorWidget(context),
                          height: height,
                          width: width,
                          placeholder: (context, url) =>
                              boxShape == BoxShape.circle
                                  ? SkeletonWidget.circular(
                                      width: width ?? 50,
                                      height: height ?? 50,
                                    )
                                  : SkeletonWidget.rectangular(
                                      width: width ?? 50,
                                      height: height ?? 50,
                                    ),
                        );
                    }
                  },
                ),
              ),
            ),
            Positioned.fill(
              child: Container(
                clipBehavior: Clip.antiAlias,
                decoration: BoxDecoration(
                  borderRadius: boxShape == BoxShape.circle
                      ? null
                      : BorderRadius.circular(borderRadius ?? 0),
                  shape: boxShape,
                  gradient: hasGradient
                      ? const LinearGradient(
                    begin: Alignment.bottomCenter,
                    end: Alignment.center,
                    tileMode: TileMode.clamp,
                    stops: [0, 0.7, 0.8],
                    colors: [
                      Colors.black,
                      Colors.black12,
                      Colors.transparent
                    ],
                  )
                      : null,
                ),
              ),
            ),
            if (hasTag)
              Align(
                alignment: tagAlignment ?? Alignment.bottomCenter,
                child: tagWidget,
              )
          ],
        ),
      ),
    );
  }

  ImageType get type {
    if (imageType != null) return imageType!;
    if (imageUrl.startsWith("http") || kIsWeb) {
      return ImageType.network;
    } else if (imageUrl.startsWith("assets")) {
      return ImageType.asset;
    }
    return ImageType.file;
  }

  Widget _errorWidget(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.all(8.0),
      child: Icon(Icons.error),
    );
  }
}
