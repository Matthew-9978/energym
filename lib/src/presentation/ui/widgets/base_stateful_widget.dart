import 'package:energym_test_project/src/presentation/constants/styles/fab_location.dart';
import 'package:energym_test_project/src/presentation/helpers/helper_functions.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/app_bar_widget.dart';
import 'package:energym_test_project/src/presentation/ui/widgets/icon_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

abstract class BaseStatefulWidget<W extends StatefulWidget, C extends Cubit>
    extends StatefulWidget {
  final bool includeHorizontalPadding;
  final bool includeVerticalPadding;
  final bool includeFab;
  final bool resizeToAvoidBottomSheet;

  const BaseStatefulWidget({
    Key? key,
    this.includeHorizontalPadding = true,
    this.includeVerticalPadding = true,
    this.resizeToAvoidBottomSheet=true,
    this.includeFab = false,
  }) : super(key: key);

  @override
  State<W> createState();
}

abstract class BaseStatefulWidgetState<T extends BaseStatefulWidget,
    C extends Cubit> extends State<T> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  ValueNotifier<bool> showFab = ValueNotifier(true);

  @override
  Widget build(BuildContext context) {
    onBuild(context);

    return GestureDetector(
      onTap: onTapScreen,
      child: WillPopScope(
        onWillPop: () async {
          return await onPop.call()?.call() ?? true;
        },
        child: Scaffold(
          key: scaffoldKey,
          backgroundColor: backgroundColor(context),
          drawer: drawer(),
          resizeToAvoidBottomInset: widget.resizeToAvoidBottomSheet,
          floatingActionButton: widget.includeFab ? fabWidget : null,
          floatingActionButtonLocation: DockedRightFabLocation(
            sizeFromBottom: fabHeight,
          ),
          appBar: appBar(context),
          bottomNavigationBar: bottomWidget(),
          body: SafeArea(
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: widget.includeHorizontalPadding ? 16 : 0,
                  vertical: widget.includeVerticalPadding ? 16 : 0),
              child: NotificationListener<UserScrollNotification>(
                onNotification: (notification) {
                  if (widget.includeFab) {
                    if (notification.direction ==
                        ScrollDirection.forward) {
                      showFab.value = true;
                    } else if (notification.direction ==
                        ScrollDirection.reverse) {
                      showFab.value = false;
                    }
                  }
                  return false;
                },
                child: body(context),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    postFrameCallback(() {
      postFrameInitialization();
    });
    super.initState();
  }

  void postFrameInitialization() => {};

  void onTapScreen() => {};

  void onBuild(BuildContext context) => {};

  Color? backgroundColor(BuildContext context) => null;

  WillPopCallback? onPop() => null;

  Widget? drawer() => null;

  Widget? bottomWidget() => null;

  String? provideRouteName() => null;

  Widget body(BuildContext context);

  CustomAppbar? appBar(BuildContext context) => null;

  Function? onPageReload(BuildContext context) => null;

  void onFabClick(BuildContext context) => () {};

  C get cubit => context.read<C>();

  double get fabHeight => 70;

  Widget get fabWidget {
    return ValueListenableBuilder<bool>(
      valueListenable: showFab,
      builder: (context, show, child) => AnimatedSlide(
        offset: show ? Offset.zero : const Offset(0, 4),
        duration: const Duration(milliseconds: 300),
        child: Builder(
          builder: (context) {
            if (widget.includeFab) {
              return FloatingActionButton(
                mini: false,
                backgroundColor: Theme.of(context).colorScheme.primary,
                elevation: 0,
                child: Center(
                  child: IconWidget(
                    icon: Icons.add,
                    iconColor: Theme.of(context).colorScheme.onBackground,
                  ),
                ),
                onPressed: () {
                  onFabClick(context);
                },
              );
            }
            return const SizedBox();
          },
        ),
      ),
    );
  }
}
