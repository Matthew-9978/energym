import 'package:energym_test_project/src/presentation/helpers/extensions/alignment_extensions.dart';
import 'package:flutter/material.dart';

class BadgeViewWidget extends StatelessWidget {
  final Widget badgeView;
  final Widget child;
  final AlignmentGeometry alignment;
  final bool showBadge;

  const BadgeViewWidget({
    Key? key,
    this.alignment = Alignment.topRight,
    this.showBadge = true,
    required this.badgeView,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      clipBehavior: Clip.none,
      children: [
        Positioned(child: child),
        if (showBadge)
          Positioned(
            top: alignment.isTop ? -6 : null,
            right: alignment.isRight ? -6 : null,
            bottom: alignment.isBottom ? -6 : null,
            left: alignment.isLeft ? -6 : null,
            child: Container(
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
              ),
              child: badgeView,
            ),
          ),
      ],
    );
  }
}
