abstract class Routes {
  Routes._();
  static const splash = _Paths.splashPage;
  static const shopPage = _Paths.shopPage;
  static const cartPage = _Paths.cartPage;
  static const productPage = _Paths.productPage;
  static const ordersPage = _Paths.ordersPage;

}

abstract class _Paths {
  static const splashPage = '/';
  static const shopPage = '/shop';
  static const cartPage = '/cart';
  static const ordersPage = '/orders';
  static const productPage = '/product';
}