import 'package:energym_test_project/src/presentation/bloc/orders_cubit/orders_cubit.dart';
import 'package:energym_test_project/src/presentation/bloc/product_cubit/product_cubit.dart';
import 'package:energym_test_project/src/presentation/bloc/shop_cubit/shop_cubit.dart';
import 'package:energym_test_project/src/presentation/bloc/splash_cubit/splash_cubit.dart';
import 'package:energym_test_project/src/presentation/config/routes.dart';
import 'package:energym_test_project/src/presentation/pages/cart/cart_page.dart';
import 'package:energym_test_project/src/presentation/pages/order/order_page.dart';
import 'package:energym_test_project/src/presentation/pages/product_page/product_page.dart';
import 'package:energym_test_project/src/presentation/pages/shop/shop_page.dart';
import 'package:energym_test_project/src/presentation/pages/splash/splash_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kiwi/kiwi.dart';

class RouteGenerator {
  static Map<String, WidgetBuilder> getRoutes(RouteSettings settings) {
    dynamic param = settings.arguments;
    KiwiContainer di = KiwiContainer();

    return {
      Routes.splash: (context) => BlocProvider(
            create: (context) => SplashCubit(),
            child: const SplashPage(),
          ),
      Routes.shopPage: (context) => BlocProvider(
            create: (context) => ShopCubit(
              productRepository: di.resolve(),
            ),
            child: const ShopPage(),
          ),
      Routes.cartPage: (context) => const CartPage(),

      Routes.ordersPage: (context) => BlocProvider(
            create: (context) => OrdersCubit(
              localStorageRepository: di.resolve()
            ),
            child: const OrdersPage(),
          ),
      Routes.productPage: (context) => BlocProvider(
        create: (context) => ProductCubit(),
        child:  ProductPage(
          params: param,
        ),
      ),
    };
  }
}
