import 'package:energym_test_project/src/domain/model/cart/cart_model.dart';
import 'package:energym_test_project/src/domain/model/product/product_model.dart';

extension CartExtensions on CartModel {
  void addProduct({
    required ProductModel product,
  }) {
    product.count = product.count + 1;
    if (isProductInCart(product)) {
      products.firstWhere((element) => element == product).count =
          product.count;
    } else {
      products.add(product);
    }
  }

  void removeProduct({
    required ProductModel product,
  }) {
    product.count = product.count - 1;
    if (product.count == 0) {
      products.removeWhere((element) => element == product);
    } else {
      products.firstWhere((element) => element == product).count =
          product.count;
    }
  }

  bool isProductInCart(ProductModel productModel) {
    return products.any((element) => element.id == productModel.id);
  }

  void updateTotalPrice() {
    totalPrice = 0;
    for (ProductModel element in products) {
      totalPrice += element.count * (element.price ?? 0);
    }
  }

  CartModel clearCart(){
    return CartModel(products: []);
  }
}
