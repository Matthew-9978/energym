class SearchableModel {
  String? name;
  String? image;
  int? id;

  SearchableModel({
    this.id,
    this.name,
    this.image,
  });
}
