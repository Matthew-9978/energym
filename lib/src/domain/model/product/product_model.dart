import 'package:energym_test_project/src/presentation/helpers/extensions/dart_extension.dart';
import 'package:equatable/equatable.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'product_model.freezed.dart';

@unfreezed
class ProductModel extends Equatable with _$ProductModel {
  ProductModel._();

  factory ProductModel({
    int? id,
    String? title,
    String? image,
    String? description,
    int? price,
    int? inventory,
    @Default(0) int count,
  }) = _ProductModel;

  factory ProductModel.clone(ProductModel model) => ProductModel(
        id: model.id,
        title: model.title,
        image: model.image,
        description: model.description,
        price: model.price,
        inventory: model.inventory,
        count: model.count,
      );

  @override
  List<Object?> get props => [id];
}
