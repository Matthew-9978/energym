import 'package:energym_test_project/src/domain/model/product/product_model.dart';
import 'package:equatable/equatable.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'cart_model.freezed.dart';

@unfreezed
class CartModel  extends Equatable with _$CartModel {
  CartModel._();

  factory CartModel({
    @Default([]) List<ProductModel> products,
    @Default(0) int totalPrice,
  }) = _CartModel;

  factory CartModel.clone(CartModel model) => CartModel(
    products: model.products,
    totalPrice: model.totalPrice,
  );

  @override
  List<Object?> get props => [products,totalPrice];
}
