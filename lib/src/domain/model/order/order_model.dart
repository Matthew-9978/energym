import 'package:energym_test_project/src/domain/model/cart/cart_model.dart';
import 'package:equatable/equatable.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'order_model.freezed.dart';

@unfreezed
class OrderModel extends Equatable with _$OrderModel {
  OrderModel._();

  factory OrderModel({
    CartModel? cart,
    DateTime? createdAt,
  }) = _OrderModel;

  factory OrderModel.clone(OrderModel model) => OrderModel(
        cart: model.cart,
        createdAt: model.createdAt,
      );

  @override
  List<Object?> get props => [cart];
}
