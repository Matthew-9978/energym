import 'package:energym_test_project/src/domain/config/env/constants.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class Environment {

  static Future<void> loadEnvironment() async  {
    if(isProduction){
      await dotenv.load(fileName: "production.env");
    }
    else{
      await dotenv.load(fileName: "development.env");
    }
  }

  static String get baseURL {
    return dotenv.get("BASE_URL");
  }

}
