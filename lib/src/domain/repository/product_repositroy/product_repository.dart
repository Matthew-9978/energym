import 'package:energym_test_project/src/domain/model/product/product_model.dart';
import 'package:energym_test_project/src/domain/model/result_model.dart';
import 'package:mockito/annotations.dart';

@GenerateMocks([ProductRepository])
abstract class ProductRepository {

  Future<ResultModel<List<ProductModel>>> getProduct();

}
