import 'package:energym_test_project/src/domain/model/cart/cart_model.dart';
import 'package:energym_test_project/src/domain/model/order/order_model.dart';
import 'package:mockito/annotations.dart';

@GenerateMocks([LocalStorageRepository])
abstract class LocalStorageRepository {
  Future<void> clearLocalStorage();

  Future<void> updateCart(CartModel cartModel);

  Future<CartModel> getCart();

  Future<void> saveOrder(OrderModel orderModel);

  Future<List<OrderModel>> getOrders();

}
