import 'package:energym_test_project/src/data/entity/product/product_entity.dart';
import 'package:energym_test_project/src/domain/model/product/product_model.dart';

extension ProductModelMapper on ProductModel{

  ProductEntity mapToEntity(){
    return ProductEntity(
      price: price,
      inventory: inventory,
      description: description,
      title: title,
      id: id,
      image: image,
      count: count,
    );
  }
}