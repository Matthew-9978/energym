import 'package:energym_test_project/src/data/entity/order/order_entity.dart';
import 'package:energym_test_project/src/domain/mapper/cart/cart_model_map_to_entity.dart';
import 'package:energym_test_project/src/domain/model/order/order_model.dart';

extension OrderModelMapper on OrderModel{

  OrderEntity mapToEntity(){
    return OrderEntity(
      createdAt: createdAt,
      cartEntity: cart?.mapToEntity()
    );
  }
}