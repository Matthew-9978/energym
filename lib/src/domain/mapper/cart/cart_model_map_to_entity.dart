import 'package:energym_test_project/src/data/entity/cart/cart_entity.dart';
import 'package:energym_test_project/src/domain/mapper/product/product_model_to_entity_mapper.dart';
import 'package:energym_test_project/src/domain/model/cart/cart_model.dart';

extension CartModelMapper on CartModel{
  CartEntity mapToEntity(){
    return CartEntity(
      products: products.map((productModel) => productModel.mapToEntity()).toList(),
      totalPrice: totalPrice,
    );
  }
}