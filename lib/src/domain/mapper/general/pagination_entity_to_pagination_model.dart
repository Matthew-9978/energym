import 'package:energym_test_project/src/data/entity/paging_resource_entity.dart';
import 'package:energym_test_project/src/domain/model/general/pagination_resource_model.dart';

extension PaginationMapper on PaginationResourceEntity {
  PaginationResourceModel<T> mapTo<D, T>({required T Function(D data) mapper}) {
    return PaginationResourceModel<T>(
      data: data.map((e) => mapper(e)).toList(),
    );
  }
}

