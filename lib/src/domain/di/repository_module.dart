import 'package:energym_test_project/src/data/repository/local_storage_repository_impl.dart';
import 'package:energym_test_project/src/data/repository/product_repository_impl.dart';
import 'package:energym_test_project/src/domain/repository/local_repository/local_storage_repository.dart';
import 'package:energym_test_project/src/domain/repository/product_repositroy/product_repository.dart';
import 'package:kiwi/kiwi.dart';

class RepositoryModule {
  static inject() {
    KiwiContainer().registerFactory<LocalStorageRepository>((container) =>
        LocalStorageRepositoryImpl(hive: container.resolve()));

    KiwiContainer().registerFactory<ProductRepository>((container) =>
        ProductRepositoryImpl(productApi: container.resolve()));
  }
}
