import 'package:bot_toast/bot_toast.dart';
import 'package:energym_test_project/src/presentation/bloc/bloc_exception_handler.dart';
import 'package:energym_test_project/src/presentation/bloc/cart_cubit/cart_cubit.dart';
import 'package:energym_test_project/src/domain/config/env/environment.dart';
import 'package:energym_test_project/src/injector.dart';
import 'package:energym_test_project/src/presentation/config/animated_page_route_builder.dart';
import 'package:energym_test_project/src/presentation/config/navigation_observer.dart';
import 'package:energym_test_project/src/presentation/constants/colors/light_theme.dart';
import 'package:energym_test_project/src/presentation/ui/widget_behavior/scroll_behavior.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_gen/gen_l10n/strings.dart';
import 'package:kiwi/kiwi.dart';
import 'package:responsive_framework/responsive_framework.dart';

final GlobalKey<NavigatorState> navigatorKey = GlobalKey();


Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Environment.loadEnvironment();
  await Injector.inject();
  Bloc.observer = BlocExceptionHandler();
  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider<CartCubit>.value(
          value: KiwiContainer().resolve<CartCubit>(),
        )
      ],
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    final botToastBuilder = BotToastInit();

    return MaterialApp(
      title: 'Nova Kudos',
      theme: lightTheme,
      debugShowCheckedModeBanner: false,
      navigatorKey: navigatorKey,
      navigatorObservers: [
        KiwiContainer().resolve<NavigationObserver>(),
        BotToastNavigatorObserver(),
      ],
      supportedLocales: const [
        Locale('en', 'US'),
      ],
      locale: const Locale('en', 'US'),
      builder: (context, child) => ResponsiveWrapper.builder(
        ScrollConfiguration(
          behavior: CustomScrollBehavior(),
          child: GestureDetector(
            onPanDown: (detail) =>
                FocusManager.instance.primaryFocus?.unfocus(),
            child: botToastBuilder(context, child),
          ),
        ),
        defaultScale: true,
        maxWidth: 900,
        breakpoints: [
          const ResponsiveBreakpoint.autoScale(450, name: MOBILE),
          const ResponsiveBreakpoint.resize(500, name: MOBILE),
        ],
      ),
      localizationsDelegates: const [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      onGenerateRoute: (settings) => AnimatedPageRouteBuilder(settings),
    );
  }
}