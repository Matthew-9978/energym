
# Energym test project
> This a test project for Energym company. it includes a shop that has several products and a shopping cart.
> Users could select products and add them to their shopping cart and finally they could save their cart as an order.


##  Getting Started

This project uses [code generation tools](https://pub.dev/packages/build_runner).<br>

**Before building the project, run these commands:**<br>

```shell script

flutter clean

```

```shell script

flutter pub get

```

```shell script

flutter pub run build_runner build --delete-conflicting-outputs

```

##  Tests

There are three types of tests in this project:
* Integration-test
* Unit-test
* Bloc-test

Test cold be run by these commands:
```shell script

/*---run integreation test ---*/
flutter test integration_test

```

```shell script

/*---run unit & bloc tests test ---*/
flutter test test

```