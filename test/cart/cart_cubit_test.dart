import 'package:bloc_test/bloc_test.dart';
import 'package:energym_test_project/src/domain/repository/local_repository/local_storage_repository.mocks.dart';
import 'package:energym_test_project/src/presentation/bloc/cart_cubit/cart_cubit.dart';
import 'package:energym_test_project/src/presentation/bloc/cart_cubit/cart_states.dart';
import 'package:energym_test_project/src/domain/model/cart/cart_model.dart';
import 'package:energym_test_project/src/domain/model/order/order_model.dart';
import 'package:energym_test_project/src/domain/model/product/product_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';


void main() {
  late MockLocalStorageRepository localStorageRepositoryMock;

  setUpAll(() async {
    localStorageRepositoryMock = MockLocalStorageRepository();
  });

  CartCubit initCubit(){
    return CartCubit(localStorageRepository: localStorageRepositoryMock);
  }

  group(
    'cart cubit tests : ',
    () {
      test("cart cubit initial state test", () {
        final cartCubit = initCubit();
        expect(cartCubit.state, const InitCartStates.init());
      });

      blocTest<CartCubit, CartStates>(
        "clear order Bloc test",
        build: () => initCubit(),
        act: (bloc)  {
          when(
            localStorageRepositoryMock.updateCart(bloc.cart),
          ).thenAnswer((_) async {});
          bloc.clearCart();
        },
        verify: (bloc) {
          verify(bloc.clearCart()).called(1);
        },
        expect: () => [
          isA<ClearingCartState>(),
        ],
      );

      blocTest<CartCubit, CartStates>(
        "getCart Bloc test",
        build: () => CartCubit(localStorageRepository: localStorageRepositoryMock),
        act: (bloc) {
          when(
            localStorageRepositoryMock.getCart(),
          ).thenAnswer(
                (_) async => CartModel(
                products: []
            ),
          );
          bloc.getCartFromStorage();
        },
        verify: (bloc) {
          verify(bloc.getCartFromStorage()).called(1);
        },
        expect: () => [
          isA<LoadingGetCartFromLocal>(),
          isA<SuccessGetCartFromLocal>(),
        ],
      );

      blocTest<CartCubit, CartStates>(
        "addProduct Bloc test",
        build: () => CartCubit(localStorageRepository: localStorageRepositoryMock),
        act: (bloc)  {
          bloc.addProduct(ProductModel(id: 2,count: 0));
        },
        verify: (bloc) {
          verify(bloc.addProduct(ProductModel(id: 2,count: 0))).called(1);
        },
        expect: () => [
          const UpdateCartState.addProduct(2, 1)
        ],
      );

      blocTest<CartCubit, CartStates>(
        "deleteProduct Bloc test",
        build: () {
          final cubit = initCubit();
          cubit.cart.products.add(ProductModel(id: 2,count: 1));
          return cubit;
        },
        act: (bloc)  {
          bloc.removeProduct(ProductModel(id: 2,count: 1));
        },
        verify: (bloc) {
          verify(bloc.removeProduct(ProductModel(id: 2,count: 1))).called(1);
        },
        expect: () => [
          const UpdateCartState.removeProduct(2, 0)
        ],
      );

      blocTest<CartCubit, CartStates>(
        "confirm order Bloc test",
        build: () {
          final cubit = initCubit();
          cubit.cart.products.add(ProductModel(id: 2,count: 1));
          return cubit;
        },
        act: (bloc)  {
          when(
            localStorageRepositoryMock.saveOrder(OrderModel(
              createdAt: DateTime.now(),
              cart: bloc.cart,
            )),
          ).thenAnswer((_) async {});
          bloc.confirmOrder();
        },
        verify: (bloc) {
          verify(bloc.confirmOrder()).called(1);
        },
        expect: () => [
          isA<LoadingConfirmOrderStates>(),
          isA<SucessConfirmOrderStates>(),
        ],
      );

    },
  );
}
