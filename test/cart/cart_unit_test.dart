import 'package:energym_test_project/src/domain/extensions/cart_extension.dart';
import 'package:energym_test_project/src/domain/model/cart/cart_model.dart';
import 'package:energym_test_project/src/domain/model/product/product_model.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  group(
    'cart unit tests : ',
    () {
      test("add product test", () async {
        CartModel cart = CartModel(products: []);
        cart.addProduct(product: ProductModel(id: 1));
        expect(cart.products.length, 1);
        cart.addProduct(product: ProductModel(id: 1, count: 1));
        expect(cart.products.length, 1);
        expect(cart.products.firstWhere((element) => element.id == 1).count, 2);
      });

      test("remove product test", () async {
        CartModel cart = CartModel(products: [
          ProductModel(id: 1, count: 2),
          ProductModel(id: 2, count: 1),
        ]);
        cart.removeProduct(product: ProductModel(id: 2, count: 1));
        expect(cart.products.length, 1);
        cart.removeProduct(product: ProductModel(id: 1, count: 2));
        expect(cart.products.firstWhere((element) => element.id == 1).count, 1);
      });

      test("clear cart test", () async {
        CartModel cart = CartModel(products: [
          ProductModel(id: 1, count: 2),
          ProductModel(id: 2, count: 1),
        ]);
        cart = cart.clearCart();
        expect(cart.products.length, 0);
      });

      test("update total price test", () async {
        CartModel cart = CartModel(products: [
          ProductModel(id: 1, count: 2,price: 100),
          ProductModel(id: 2, count: 1,price: 200),
        ]);
        cart.updateTotalPrice();
        expect(cart.totalPrice, 400);
      });

      test("is product in cart test", () async {
        CartModel cart = CartModel(products: [
          ProductModel(id: 1),
          ProductModel(id: 2),
        ]);
        expect(cart.isProductInCart(ProductModel(id: 1)), true);
        expect(cart.isProductInCart(ProductModel(id: 6)), false);
      });
    },
  );
}
